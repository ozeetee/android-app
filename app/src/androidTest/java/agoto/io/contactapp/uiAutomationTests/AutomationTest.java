package agoto.io.contactapp.uiAutomationTests;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.squareup.okhttp.mockwebserver.MockResponse;
import com.squareup.okhttp.mockwebserver.MockWebServer;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import agoto.io.contactapp.ContactListActivity;
import agoto.io.contactapp.R;
import agoto.io.contactapp.TestHelper;
import agoto.io.contactapp.restapi.ApiClient;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * By GT.
 *
 * UI automation test cases for testing user flows
 */
@RunWith(AndroidJUnit4.class)
public class AutomationTest {

    //Mockwebserver for mocking server responses
    private MockWebServer server;

    @Rule
    public ActivityTestRule<ContactListActivity> mActivityRule = new ActivityTestRule<>(ContactListActivity.class, true, false);


    @Before
    public void setUp() throws Exception {
        server = new MockWebServer();
        server.start();
        ApiClient.BASE_URL = server.url("/").toString();
    }

    @Test
    public void testSortDesc() throws Exception{
        String fileName = "contacts_200_ok.json";
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(TestHelper.getStringFromFile(InstrumentationRegistry.getContext(), fileName)));
        mActivityRule.launchActivity(new Intent());

        String sortDescLabel =InstrumentationRegistry.getTargetContext().getString(R.string.sort_desc);
        //Open options menu
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getTargetContext());
        onView(withText(sortDescLabel)).perform(click());

        //Check username is visible in the details activity
        onView(withText("Patricia Lebsack")).check(matches(isDisplayed()));
    }

    @Test
    public void testContactDetailsActivityLaunched() throws Exception{
        String fileName = "contacts_200_ok.json";
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(TestHelper.getStringFromFile(InstrumentationRegistry.getContext(), fileName)));
        mActivityRule.launchActivity(new Intent());

        //This will launch the details activity
        onView(withText("Clementine Bauch")).perform(click());

        //Check username is visible in the details activity
        onView(withText("Samantha")).check(matches(isDisplayed()));
    }


    @After
    public void tearDown() throws Exception {
        server.shutdown();
    }

}
