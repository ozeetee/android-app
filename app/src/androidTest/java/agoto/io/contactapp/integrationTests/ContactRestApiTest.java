package agoto.io.contactapp.integrationTests;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import agoto.io.contactapp.models.Contact;
import agoto.io.contactapp.restapi.ApiClient;
import agoto.io.contactapp.restapi.ApiInterface;
import retrofit2.Call;
import retrofit2.Response;
import static org.junit.Assert.*;
/**
 * By GT.
 */
@RunWith(AndroidJUnit4.class)
public class ContactRestApiTest {

    @Test
    public void testFetchContact() throws Exception{
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<List<Contact>> call = apiService.getContacts();
        Response<List<Contact>> response = call.execute();
        assertNotNull(response);
        assertNotNull(response.body());
        assertEquals(200,response.raw().code());
    }
}
