package agoto.io.contactapp.instrumentedTests;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.squareup.okhttp.mockwebserver.MockResponse;
import com.squareup.okhttp.mockwebserver.MockWebServer;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import agoto.io.contactapp.ContactListActivity;
import agoto.io.contactapp.R;
import agoto.io.contactapp.TestHelper;
import agoto.io.contactapp.restapi.ApiClient;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * By GT.
 */
@RunWith(AndroidJUnit4.class)
public class ContactListActivityTest{

    //Mockwebserver for mocking server responses
    private MockWebServer server;

    @Rule
    public ActivityTestRule<ContactListActivity> mActivityRule = new ActivityTestRule<>(ContactListActivity.class, true, false);

    @Before
    public void setUp() throws Exception {
        server = new MockWebServer();
        server.start();
        ApiClient.BASE_URL = server.url("/").toString();
    }

    @Test
    public void testRetryButtonOn404() throws Exception{
        server.enqueue(new MockResponse().setResponseCode(404));
        mActivityRule.launchActivity(new Intent());
        onView(withId(R.id.btn_retry)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }

    @Test
    public void testRetryButtonOn500() throws Exception{
        server.enqueue(new MockResponse().setResponseCode(500));
        mActivityRule.launchActivity(new Intent());
        onView(withId(R.id.btn_retry)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }


    @Test
    public void testRetryButtonOnMalformedResponseFromServer() throws Exception{
        String fileName = "contacts_200_malformed.json";
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(TestHelper.getStringFromFile(InstrumentationRegistry.getContext(), fileName)));
        mActivityRule.launchActivity(new Intent());
        onView(withId(R.id.btn_retry)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }

    @Test
    public void test200OkResponse() throws Exception{
        String fileName = "contacts_200_ok.json";
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(TestHelper.getStringFromFile(InstrumentationRegistry.getContext(), fileName)));
        mActivityRule.launchActivity(new Intent());
        onView(withId(R.id.btn_retry)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
        onView(withId(R.id.ll_loading_contacts)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
        onView(withId(R.id.contact_list)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
        //Check first item is displayed in recycler view
        onView(withText("Clementine Bauch")).check(matches(isDisplayed()));
    }


    @After
    public void tearDown() throws Exception {
        server.shutdown();
    }

}
