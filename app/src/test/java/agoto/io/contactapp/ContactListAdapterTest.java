package agoto.io.contactapp;

import org.junit.Before;
import org.junit.Test;

import agoto.io.contactapp.models.Contact;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
/**
 * By GT.
 */
public class ContactListAdapterTest {

    private ContactListAdapter listAdapter;

    @Before
    public void initializeAdapter(){
        listAdapter = new ContactListAdapter(TestUtils.getContactList(), new ContactListAdapter.ContactClickListerner() {
            @Override
            public void onContactClicked(Contact contact) {
                //Do nothing
            }
        });
    }

    @Test
    public void testAdapterInitialize() throws Exception{
        assertNotNull(listAdapter.getContacts());
        assertEquals("Gaurav Tiwari",listAdapter.getContacts().get(0).getName());
    }

    @Test
    public void testSortAsc() throws Exception{
        assertNotNull(listAdapter.getContacts());
        try {
            listAdapter.sortAsc();
        } catch (Exception e) {
            //Do nothing
        }
        assertEquals("Gaurav Tiwari",listAdapter.getContacts().get(0).getName());
    }

    @Test
    public void testSortDesc() throws Exception{
        assertNotNull(listAdapter.getContacts());
        try {
            listAdapter.sortDesc();
        } catch (Exception e) {
            //Do nothing
        }
        assertEquals("Zlenna Reichert",listAdapter.getContacts().get(0).getName());
    }

}
