package agoto.io.contactapp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import agoto.io.contactapp.models.Contact;

/**
 * By GT.
 */
public class TestUtils {

    public static Contact getContact1(){
        Contact c = new Contact();
        c.setName("Gaurav Tiwari");
        return c;
    }

    public static Contact getContact2(){
        Contact c = new Contact();
        c.setName("Kattie");
        return c;
    }

    public static Contact getContact3(){
        Contact c = new Contact();
        c.setName("Zlenna Reichert");
        return c;
    }

    public static Contact getContact4(){
        Contact c = new Contact();
        c.setName("Patricia Lebsack");
        return c;
    }


    public static List<Contact> getContactList(){
        List<Contact> contacts = new ArrayList<>();
        contacts.add(getContact1());
        contacts.add(getContact2());
        contacts.add(getContact3());
        contacts.add(getContact4());
        Collections.shuffle(contacts); // Shuffle the list
        return contacts;
    }

}
