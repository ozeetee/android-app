package agoto.io.contactapp;

import org.junit.Test;

import agoto.io.contactapp.models.Contact;

import static org.junit.Assert.*;

/**
 * By GT.
 */
public class ContactsAppUnitTest {


    @Test
    public void testUserNameInitials() throws Exception {
        Contact c = TestUtils.getContact1();
        String nameInitial = Utils.initials(c);
        assertEquals("GT",nameInitial);
    }

    @Test
    public void testUserNameInitialsTwo() throws Exception {
        Contact c = TestUtils.getContact2();
        String nameInitial = Utils.initials(c);
        assertEquals("K",nameInitial);
    }




}
