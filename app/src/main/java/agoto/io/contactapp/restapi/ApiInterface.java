package agoto.io.contactapp.restapi;

import java.util.List;

import agoto.io.contactapp.models.Contact;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by GT
 */
public interface ApiInterface {

    @GET("users")
    Call<List<Contact>> getContacts();
}
