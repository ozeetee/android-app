package agoto.io.contactapp.views;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import agoto.io.contactapp.R;
import agoto.io.contactapp.Utils;
import agoto.io.contactapp.models.Contact;


/**
 * @author GT
 */
public class ProfilePicView extends RelativeLayout {

    private ImageView mProfilePic;
    private TextView mUserNameInitial;
    private Contact contact;

    private float mCornerRadius;
    private int[] colorsArr;
    private GradientDrawable[] bgDrawables;
    private int whiteColor;



    public ProfilePicView(Context context) {
        super(context);
        init();
    }

    public ProfilePicView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ProfilePicView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.user_profile_pic, this);
        this.mProfilePic = (ImageView) findViewById(R.id.iv_profile_pic);
        this.mUserNameInitial = (TextView)findViewById(R.id.tv_user_name_initials);
        this.colorsArr = getResources().getIntArray(R.array.user_pic_colors);

        mCornerRadius = getResources().getDimension(R.dimen.corner_radius);
        //Create and cache color drawables
        int len = colorsArr.length;
        bgDrawables = new GradientDrawable[len];
        for (int i = 0; i < len; i++){
            GradientDrawable gd = new GradientDrawable();
            gd.setColor(colorsArr[i]);
            gd.setCornerRadius(mCornerRadius);
            bgDrawables[i] = gd;
        }
        whiteColor = getResources().getColor(R.color.white);
    }

    public void showImageWithContact(Contact contact){
        this.contact = contact;
        setUserNameInitials();
        showColorDrawable();
    }

    private void showColorDrawable(){
        int hashCode = contact.hashCode();
        int offset = Math.abs(hashCode % colorsArr.length);
        GradientDrawable bgColorDrawable = bgDrawables[offset];
        mProfilePic.setBackgroundDrawable(bgColorDrawable);
    }

    private void setUserNameInitials() {
        mUserNameInitial.setText(Utils.initials(contact));
    }
}
