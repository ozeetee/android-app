package agoto.io.contactapp;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.commons.lang3.StringUtils;

import agoto.io.contactapp.models.Contact;


/**
 * A fragment representing a single Contact detail screen.
 * This fragment is either contained in a {@link ContactListActivity}
 * in two-pane mode (on tablets) or a {@link ContactDetailActivity}
 * on handsets.
 */
public class ContactDetailFragment extends Fragment implements OnMapReadyCallback {

    public static final String ARG_CONTACT = "contact";
    private Contact contact;

    //  personal details
    private TextView mUserName;
    private TextView mEmail;
    private TextView mPhone;
    private TextView mWebsite;

    // Address
    private TextView mAddress;
    private MapView mapView;
    private GoogleMap map;


    //Company details
    private View mCompanyDetails;
    private View mAddressDetails;

    private TextView mCompany;

    private int colorMedium;
    private int colorUltraDark;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        colorMedium = ContextCompat.getColor(getContext(),R.color.medium);
        colorUltraDark = ContextCompat.getColor(getContext(),R.color.ultra_dark);

        if(savedInstanceState != null && savedInstanceState.containsKey(ARG_CONTACT)){
            contact = (Contact) savedInstanceState.getSerializable(ARG_CONTACT);
        }else if (getArguments().containsKey(ARG_CONTACT)) {
            contact = (Contact) getArguments().getSerializable(ARG_CONTACT);
        }

        if(contact != null){
            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(contact.getName());
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(contact != null){
            outState.putSerializable(ARG_CONTACT, contact);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.contact_detail, container, false);

        //Card Views
        mCompanyDetails = rootView.findViewById(R.id.cv_company_details);
        mAddressDetails = rootView.findViewById(R.id.cv_address_details);

        mUserName = (TextView) rootView.findViewById(R.id.userName);
        mEmail = (TextView) rootView.findViewById(R.id.email);
        mPhone = (TextView) rootView.findViewById(R.id.phone);
        mWebsite = (TextView) rootView.findViewById(R.id.website);
        mAddress = (TextView) rootView.findViewById(R.id.address);
        mCompany = (TextView) rootView.findViewById(R.id.company);
        mapView = (MapView) rootView.findViewById(R.id.addressMap);

        if (contact != null) {
            if(StringUtils.isNotEmpty(contact.getUsername())) mUserName.setText(contact.getUsername());
            if(StringUtils.isNotEmpty(contact.getEmail())) mEmail.setText(contact.getEmail());
            if(StringUtils.isNotEmpty(contact.getPhone())) mPhone.setText(contact.getPhone());
            if(StringUtils.isNotEmpty(contact.getWebsite())) mWebsite.setText(contact.getWebsite());
            showAddress();
            showCompanyDetails();
        }
        return rootView;
    }


    private void showAddress() {
        if(contact.getAddress() == null){
            mAddressDetails.setVisibility(View.GONE);
            return;
        }
        //Showing address on map
        initializeMapView();

        StringBuilder sb = new StringBuilder();
        if(StringUtils.isNotEmpty(contact.getAddress().getSuite())) sb.append(contact.getAddress().getSuite()).append("\n");
        if(StringUtils.isNotEmpty(contact.getAddress().getStreet())) sb.append(contact.getAddress().getStreet()).append("\n");
        if(StringUtils.isNotEmpty(contact.getAddress().getCity())) sb.append(contact.getAddress().getCity());
        if(StringUtils.isNotEmpty(contact.getAddress().getZipcode())) sb.append(" - ").append(contact.getAddress().getZipcode());
        mAddress.setText(sb.toString());
    }

    private void showCompanyDetails(){
        if(contact.getCompany() == null){
            mCompanyDetails.setVisibility(View.GONE);
            return;
        }

        SpannableStringBuilder sb = new SpannableStringBuilder();

        if(StringUtils.isNotEmpty(contact.getCompany().getName())){
            sb.append(contact.getCompany().getName());
            sb.setSpan(new StyleSpan(Typeface.BOLD),0,sb.length(),0);
            sb.setSpan(new ForegroundColorSpan(colorUltraDark),0,sb.length(),0);
            sb.append("\n");
        }
        if(StringUtils.isNotEmpty(contact.getCompany().getCatchPhrase())) sb.append(contact.getCompany().getCatchPhrase()).append("\n");
        if(StringUtils.isNotEmpty(contact.getCompany().getBs())){
            int start = sb.length()-1;
            sb.append(contact.getCompany().getBs());
            sb.setSpan(new ForegroundColorSpan(colorMedium),start,sb.length(),0);
        }
        mCompany.setText(sb);
    }


    /**
     * Initialises the MapView by calling its lifecycle methods.
     */
    private void initializeMapView() {
        if(contact.getAddress() == null || contact.getAddress().getGeo() == null || !Utils.isGooglePlayServicesAvailable(getContext())){
            mapView.setVisibility(View.GONE);
            return;
        }

        if (mapView != null) {
            // Initialise the MapView
            mapView.onCreate(null);
            // Set the map ready callback to receive the GoogleMap object
            mapView.getMapAsync(this);
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(this.getActivity());
        map = googleMap;
        LatLng latLang = new LatLng(contact.getAddress().getGeo().getLat(), contact.getAddress().getGeo().getLat());
        setMapLocation(latLang);
    }

    private void setMapLocation(LatLng location) {
        // Add a marker for this item and set the camera
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 3f));
        map.addMarker(new MarkerOptions().position(location));

        // Set the map type back to normal.
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

}
