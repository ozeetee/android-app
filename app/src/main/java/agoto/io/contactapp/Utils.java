package agoto.io.contactapp;

import android.content.Context;
import android.content.res.Resources;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;

import agoto.io.contactapp.models.Contact;

/**
 * Created by GT
 */
public class Utils {

    /**
     * convert dp to px
     */
    public static int dpToPx(float dp) {
        float density = Resources.getSystem().getDisplayMetrics().density;
        return (int)  (dp * density);
    }

    public static String initials(Contact contact){
        if(contact == null || StringUtils.isBlank(contact.getName())) return "";
        String initials = WordUtils.initials(contact.getName());
        if(initials.length() >= 2){
            return initials.substring(0,2);
        }else{
            return initials;
        }
    }

    /**
     * Check if the google play services is available on a device. To show google maps for location
     *
     * @param context
     * @return true if google play services is available
     */
    public static boolean isGooglePlayServicesAvailable(Context context){
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = googleApiAvailability.isGooglePlayServicesAvailable(context);
        return resultCode == ConnectionResult.SUCCESS;
    }
}
