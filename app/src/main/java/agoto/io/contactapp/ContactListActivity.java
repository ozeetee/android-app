package agoto.io.contactapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import agoto.io.contactapp.models.Contact;
import agoto.io.contactapp.restapi.ApiClient;
import agoto.io.contactapp.restapi.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * An activity representing a list of Contacts. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link ContactDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class ContactListActivity extends AppCompatActivity implements Callback<List<Contact>>, ContactListAdapter.ContactClickListerner {

    private static final String KEY_CONTACTS = "key_contacts";

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;
    private RecyclerView recyclerView;
    private boolean isDataLoaded = false;
    private ContactListAdapter contactListAdapter;

    private View mLoadingContactContainer;
    private View mErrorContainer;
    private Button btnRetry;

    private List<Contact> contacts;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        mLoadingContactContainer = findViewById(R.id.ll_loading_contacts);
        mErrorContainer = findViewById(R.id.ll_contact_error);
        btnRetry = (Button) findViewById(R.id.btn_retry);

        recyclerView = (RecyclerView)findViewById(R.id.contact_list);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));
        if (findViewById(R.id.contact_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        if(savedInstanceState != null && savedInstanceState.containsKey(KEY_CONTACTS)){
            contacts = (ArrayList<Contact>) savedInstanceState.getSerializable(KEY_CONTACTS);
            initializeContactList();
        }else {
            fetchContact();
        }

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fetchContact();
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(contacts != null){
            outState.putSerializable(KEY_CONTACTS, (ArrayList<Contact>)contacts);
        }
    }

    @Override
    public void onResponse(Call<List<Contact>> call, Response<List<Contact>> response) {
        // When restapi request success
        if(response != null && response.raw().code() == 200){
            contacts = response.body();
            initializeContactList();
        }else {
            showError();
        }
    }

    private void initializeContactList(){
        isDataLoaded = true;
        showContact();
        contactListAdapter = new ContactListAdapter(contacts,this);
        recyclerView.setAdapter(contactListAdapter);
        invalidateOptionsMenu();
    }

    @Override
    public void onFailure(Call<List<Contact>> call, Throwable t) {
        // When restapi request failed
        showError();
    }


    private void fetchContact(){
        isDataLoaded = false;
        showLoading();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<List<Contact>> call = apiService.getContacts();
        call.enqueue(this);
        invalidateOptionsMenu();
    }


    private void showLoading(){
        mLoadingContactContainer.setVisibility(View.VISIBLE);
        mErrorContainer.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
    }

    private void showError(){
        mLoadingContactContainer.setVisibility(View.GONE);
        mErrorContainer.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    private void showContact(){
        mLoadingContactContainer.setVisibility(View.GONE);
        mErrorContainer.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }


    @Override
    public void onContactClicked(final Contact contact) {
        if (mTwoPane) {
            Bundle arguments = new Bundle();
            arguments.putSerializable(ContactDetailFragment.ARG_CONTACT, contact);
            ContactDetailFragment fragment = new ContactDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.contact_detail_container, fragment)
                    .commit();
        } else {
            Intent intent = new Intent(this, ContactDetailActivity.class);
            intent.putExtra(ContactDetailFragment.ARG_CONTACT, contact);
            this.startActivity(intent);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(!isDataLoaded) return true;
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(!isDataLoaded){
            return true;
        }

        int id = item.getItemId();

        if (id == R.id.sort_asc) {
            if(contactListAdapter != null) contactListAdapter.sortAsc();
            return true;
        }

        if(id == R.id.sort_desc){
            if(contactListAdapter != null) contactListAdapter.sortDesc();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
