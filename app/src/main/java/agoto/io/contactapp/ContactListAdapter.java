package agoto.io.contactapp;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import agoto.io.contactapp.models.Contact;
import agoto.io.contactapp.views.ProfilePicView;

/**
 * Created by GT
 */
public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ContactViewHolder>{


    private final List<Contact> contacts;
    private ContactClickListerner contactClickListerner;

    public ContactListAdapter(List<Contact> contacts, ContactClickListerner contactClickListerner) {
        if(contacts == null){
            this.contacts = new ArrayList<>();
        }else {
            this.contacts = contacts;
        }
        //By default sort the contact by ascending order
        Collections.sort(contacts, new Comparator<Contact>() {
            @Override
            public int compare(Contact c1, Contact c2) {
                return c1.getName().compareTo(c2.getName());
            }
        });

        this.contactClickListerner = contactClickListerner;
    }


    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_list_content, parent, false);
        return new ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ContactViewHolder holder, int position) {
        Contact c = contacts.get(position);
        holder.contact = c;
        holder.mContactName.setText(c.getName());
        holder.mContactEmail.setText(c.getEmail());
        holder.mProfilePicView.showImageWithContact(c);
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(contactClickListerner != null) contactClickListerner.onContactClicked(holder.contact);
            }
        });
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    public void sortAsc() {
        Collections.sort(contacts, new Comparator<Contact>() {
            @Override
            public int compare(Contact c1, Contact c2) {
                return c1.getName().compareTo(c2.getName());
            }
        });
        notifyDataSetChanged();
    }

    public void sortDesc() {
        Collections.sort(contacts, new Comparator<Contact>() {
            @Override
            public int compare(Contact c1, Contact c2) {
                return c2.getName().compareTo(c1.getName());
            }
        });
        notifyDataSetChanged();
    }


    public class ContactViewHolder extends RecyclerView.ViewHolder{
        public final View mView;
        public final TextView mContactName;
        public final TextView mContactEmail;
        public final ProfilePicView mProfilePicView;
        public Contact contact;

        public ContactViewHolder(View view) {
            super(view);
            mView = view;
            mContactName = (TextView) view.findViewById(R.id.tvContactName);
            mContactEmail = (TextView) view.findViewById(R.id.tvContactEmail);
            mProfilePicView = (ProfilePicView) view.findViewById(R.id.profile_pic);
        }
    }

    public interface ContactClickListerner{
        void onContactClicked(Contact contact);
    }

    //For efficient unit testing
    public List<Contact> getContacts() {
        return contacts;
    }
}
