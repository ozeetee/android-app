# Contacts App Readme #

### Useful Links ###
* [Source Code](https://bitbucket.org/ozeetee/android-app/src) 
* [APK File](https://bitbucket.org/ozeetee/android-app/downloads/contact-app.apk) 
* You can also find the downloadable apk `contact-app.apk` in the [downloads](https://bitbucket.org/ozeetee/android-app/downloads) section of this repository.
* [Detail Documentation](https://bitbucket.org/ozeetee/android-app/wiki/Home) 

### To run the app in emulator ###
* Run Android Emulator on your system.
* Download [contact-app.apk](https://bitbucket.org/ozeetee/android-app/downloads/contact-app.apk) file. 
* Drag and drop the downloaded apk to the emulator window.

### To set up the code ###

* Clone the repository :  `git clone git@bitbucket.org:ozeetee/android-app.git`
* Open the project in Android studio. : `File > Open` menu
* Run the app using `run` button or `Run > Run app ` menu 

### Test Cases ###
* Unit test cases : `src/test/java`
* Instrumented Test cases : `src/androidTest/java agoto.io.contactapp.instrumentedTests` 
* Integration Test cases : `src/androidTest/java agoto.io.contactapp.integrationTests`
* UI Automation Test cases : `src/androidTest/java agoto.io.contactapp.uiAutomationTests`


### Libraries Used ###

* [Android Support Libraries](https://developer.android.com/topic/libraries/support-library/features.html)
* [Google maps](https://developers.google.com/maps/documentation/android-api/start) for showing address location.
* [Retrofit](http://square.github.io/retrofit/) for making rest api calls. 
* [MaterialProgressBar](https://github.com/DreaminginCodeZH/MaterialProgressBar)

### Developer ###

* Gaurav Tiwari